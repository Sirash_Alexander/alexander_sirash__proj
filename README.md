# Branches:

## feature/gulp-markup 
> set gulp tasks with partial markup

## feature/webpack-auth
> set webpack

## feature/search
> json placeholder search (fetch)

## feature/oop
> inheritance (es5/es6)

## feature/react-redux
> react with redux

## feature/angular
> angular

## feature/table-generator
> parse csv data and render table

## feature/future-dialog-cards
> markup
